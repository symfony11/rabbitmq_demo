## What is docker-compose ?
`docker-compose` is a wrapper around `docker` that takes as input a configuration file named `docker-compose.yml`. It allows us to specify multiple containers and their relations once and deploy on the fly.

## How to build the docker-compose config file ?
```docker-compose build```

## How to actually launch the built images ?
```docker-compose up -d``` (to launch images in the background)

## How to list running containers ?
```docker-compose ps```

## How to get a shell from a running container ?
```docker-compose exec <CONTAINER_NAME> <FULL_PATH_OF_THE_SHELL>```

For example, for a container named rabbitmq based on debian, we have:
```docker-compose exec rabbitmq /bin/bash```
